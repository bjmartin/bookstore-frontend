console.log("Hello World!");

const BACKEND_URL = "http://18.219.46.139:5050/books";
//const BACKEND_URL ="http://localhost:5050/books"   
//const request = new Request(BACKEND_URL, {method: 'GET'});

async function fetchData()
{
    const request = new Request(BACKEND_URL, {method: 'GET'});
    const response = await fetch(request);
    const result = await response.json();
    //console.log(result);

    document.getElementById('table').innerHTML = "";


    let htmlCode = '<div class= "container"> <div class = "row">';
    for(var i = 0; i < result.length; i++){
        //console.log(result[i]);
        htmlCode += '<div class= "col-md-3 border border-warning rounded">';

        //referenced by stackoverflow.
        htmlCode += ' <center><br> <img  src=' + getRandomImage() + ' alt="pciture" width = "110" height = "150" class="mr-4 rounded"><center>';
        
        htmlCode += '<br>'
        htmlCode += '<center><label><strong>Title: </strong>' + result[i].title + '</label><center><br>'
        htmlCode += ' <center><label><strong>Author: </strong>' + result[i].author + '</label><center><br>';
        htmlCode += ' <center><label><strong>Price: </strong>$' + result[i].price + '</label><center>';  
        htmlCode += ' <center><br><button class = "button1" onclick=deleteData(\'' + result[i]._id  + '\')>DELETE</button></center>';
        htmlCode += ' <center><br><button class = "button2" onclick=editData(\'' + result[i]._id + '\')>UPDATE</button></center>';

        htmlCode += '</div>'

    }
        htmlCode += '</div> </div>';
    document.getElementById('table').innerHTML += htmlCode;
};

async function postData(){

    const data = {title: '', author: '', price: 0}

    data.title = document.getElementById('title').value;
    data.author = document.getElementById('author').value;
    data.price = document.getElementById('price').value;

    const request = new Request(BACKEND_URL, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });

    const response = await fetch(request);
    if (response.status === 400){
        console.log("It failed! Error: ");
    }

    else if (response.status === 500){
        console.log("Server Error");
    }

    const result = await response.json();
    console.log("Record Inserted Successfully!");
    console.log(result);

    fetchData();

}

async function deleteData(id = ''){

    //console.log("Hello");

    const data = {id}

    const request = new Request(BACKEND_URL +'/'+ id , {
        method: 'DELETE',
        headers: {'Accept' : 'application/json',
            'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });

    const response = await fetch(request);
    if (response.status === 400){
        console.log("It failed! Error: ");
    }
 
    else if (response.status === 500){
        console.log("Server Error");
    }

    const result = await response.json();
    console.log(result);

    fetchData();

} 

async function editData(id = ''){

    const data = {id, title: '', author: '', price: 0}

    data.title = document.getElementById('title').value;
    data.author = document.getElementById('author').value;
    data.price = document.getElementById('price').value;
   
    const request = new Request(BACKEND_URL +'/'+ id , {
        method: 'PUT',
        headers: {'Accept' : 'application/json',
            'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    });

    const response = await fetch(request);
    if (response.status === 400){
        console.log("It failed! Error: ");
    }

    else if (response.status === 500){
        console.log("Server Error");
    }

    const result = await response.json();
    console.log(result);

    fetchData();
}